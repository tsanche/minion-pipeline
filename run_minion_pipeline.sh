#!/bin/bash

start=$(date +%s)
export TERM=xterm-256color
COLOR="\e[35m"
ENDCOLOR="\e[0m"

export WORKING_DIRECTORY=/data/users/theophile/minion-pipeline/
export DATASET_NAME=mini_ve16S
export POD5_DIRECTORY=/data/users/theophile/minion-pipeline/data/mini_Ve16S/20230608_0140_MC-114986_FAW57934_c0e11e80/pod5
export OUTPUT_DIRECTORY=/data/users/theophile/minion-pipeline/data/Ve16S_pipeline_output
export REFERENCE_DATABASE_DIRECTORY=/data/users/theophile/minion-pipeline/data/reference_database/MIDORI2_TOTAL_NUC_GB256_lrRNA_RAW.fasta
threads=6
min_phred_qual=10

start_date=$(date +"%d%m%Y_%H%M%S")
log_info() {
    echo -e "[${COLOR}MinION pipeline${ENDCOLOR}] $(date '+%d-%m-%Y %H:%M:%S'): $1"
}

log_info "Starting MinION eDNA pipeline"
fastq_directory="$OUTPUT_DIRECTORY/"$DATASET_NAME"_$start_date/fastq"
mkdir -p $fastq_directory
basecalling_commands="
    cd $WORKING_DIRECTORY &&
    mkdir -p $OUTPUT_DIRECTORY &&
    dorado download --model dna_r10.4.1_e8.2_400bps_hac@v4.1.0 &&
    dorado basecaller dna_r10.4.1_e8.2_400bps_hac\@v4.1.0 $POD5_DIRECTORY --emit-fastq \
    > "$fastq_directory"/"$DATASET_NAME"_$start_date.fastq"

log_info "Starting basecalling on $POD5_DIRECTORY"
docker run --rm --gpus=all \
    -v "$WORKING_DIRECTORY":"$WORKING_DIRECTORY" \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    --user $(id -u):$(id -g) \
    ontresearch/dorado /bin/bash -c "$basecalling_commands" 

log_info "Basecalling over, results are logged in "$OUTPUT_DIRECTORY"/"$DATASET_NAME"_"$start_date""

log_info "Removing sequence with phred quality lower than "$min_phred_qual""
filtered_fastq_directory="$OUTPUT_DIRECTORY/"$DATASET_NAME"_$start_date/filtered_fastq"
mkdir -p $filtered_fastq_directory
for file in $fastq_directory/*
do
  #cat "$file"  | docker run --rm thesanc/chopper chopper -q $min_phred_qual --threads $threads > "$filtered_fastq_directory"/"$(basename $file)"
  docker run --rm \
    -v "$WORKING_DIRECTORY":"$WORKING_DIRECTORY" \
    -v /etc/passwd:/etc/passwd:ro \
    -v /etc/group:/etc/group:ro \
    --user $(id -u):$(id -g) \
    thesanc/chopper bash -c "cat "$file" | chopper -q $min_phred_qual --threads $threads > "$filtered_fastq_directory"/"$(basename $file)""
done

log_info "Compressing fastq file in "$fastq_directory" and "$filtered_fastq_directory""
gzip $fastq_directory/*
gzip $filtered_fastq_directory/*
log_info "Compression over"

log_info "Downloading nextflow"
wget -qO- https://get.nextflow.io | bash 
chmod +x nextflow

log_info "Starting taxonomic assignation with wf-metagenomics"
wf_directory="$OUTPUT_DIRECTORY/"$DATASET_NAME"_$start_date/wf_results"
wf_working_directory="$OUTPUT_DIRECTORY/"$DATASET_NAME"_$start_date/wf_work"
mkdir -p $wf_directory $wf_working_directory
#bash nextflow run epi2me-labs/wf-metagenomics -w $wf_working_directory --threads 6 --fastq $fastq_directory --database_set SILVA_138_1 --classifier kraken2 --store_dir $wf_directory --out_dir $wf_directory

#bash nextflow run epi2me-labs/wf-metagenomics -w $wf_working_directory --database_set SILVA_138_1 --threads $threads --fastq $filtered_fastq_directory \
#--classifier minimap2 --store_dir $wf_directory --out_dir $wf_directory

bash nextflow run epi2me-labs/wf-metagenomics -w $wf_working_directory --reference $REFERENCE_DATABASE_DIRECTORY --threads $threads --fastq $filtered_fastq_directory \
--classifier minimap2 --store_dir $wf_directory --out_dir $wf_directory

end=$(date +%s)
log_info "End of pipeline"
log_info "Elapsed Time: $(($end-$start)) seconds"


#bash nextflow run epi2me-labs/wf-metagenomics -w test --reference  --threads 4 --fastq /data/users/theophile/minion-pipeline/data/Ve16S_pipeline_output/ve16S_02102023_114904/filtered_fastq/ve16S_02102023_114904.fastq.gz
#--classifier minimap2 --store_dir test --out_dir test